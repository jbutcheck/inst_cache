#!/usr/bin/env python3.5
'''
File: mongo.py
Auth: Jeremy Butcheck
Orig: 08/23/16
Project: Baldur
'''

import motor.motor_asyncio as motor

def mongo_client(cfg):
    conn_str = cfg.get('mongo','conn_str')
    replset = cfg.get('mongo', 'replset')

    client = motor.AsyncIOMotorReplicaSetClient(conn_str, replicaSet=replset)

    return client 


def mongo_collection(cfg):
    collection_name = cfg.get('mongo', 'collection')

    client = mongo_client(cfg)

    return client[collection_name]
