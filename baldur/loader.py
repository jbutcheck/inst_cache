#!/usr/bin/env python3.5
'''
File: loader.py
Auth: Jeremy Butcheck
Orig: 07/18/16
Project: Baldur
'''

import parse_info_cfg


def _pt_to_dict(self, pt):
    # Converts prop trees returned from parse_info_cfg into default 
    # python dictionaries.
    if pt.data is not None:
        return pt.data

    else:
        attributes = {}
        for child_name, child in pt:
            attributes[child_name] = pt_to_dict(child)

        return attributes


def get_insts(fname):
    # Creates a python dictionary of instrument data using instrument ids
    # as keys.
    print('Reading instruments...')
    pts = parse_info_cfg.read_info_cfg(fname)

    print('DONE\nTranslating instruments...')

    insts = {} 
    for inst_id, pt in pts:
        insts[inst_id] = pt_to_dict(pt)

    print('DONE')

    return insts


async def insert_insts(insts, collection):
    # Inserts instrument data into mongo colleciton.
    print('Inserting instruments...')

    for inst_id , inst in insts.items():
        inst["_id"] = inst_id
        result = await self.collection.save(inst)

    print('DONE')

