#!/usr/bin/env python3.5
'''
File: logging.py
Auth: Jeremy Butcheck
Orig: 08/23/16
Project: Baldur
'''

from smtplib import SMTP as smtp

def send_email(context, ex):
    host = context.get('email', 'host')
    port = context.get('email', 'port')
    sndr = context.get('email', 'sndr')
    rcpt = context.get('email', 'rcpt')

    sub  = 'Instrument Loading Failed'
    header = 'From{sndr}:\nTo:{rcpt}\nSubject:{sub}\n'.format(sndr=sndr,
                                                              rcpt=rcpt,
                                                              sub=sub)
    body = str(ex)

    try:
        s = smtp(host, port)
        s.sendmail(sndr, [rcpt], header+body)

    except Exception as ex:
        error_msg = 'Failed to send email.\n{} {} {} {}'.format(host, port, 
                                                                sndr, rcpt)
        print(error_msg, file=sys.stderr)
