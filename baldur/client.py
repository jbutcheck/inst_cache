#!/usr/bin/env python3.5
'''
File: client.py
Auth: Jeremy Butcheck
Orig: 08/22/16
Project: Baldur
'''

import asyncio
import json


class Client(asyncio.Protocol):
    # TCP/IP socket connection.  Used with server from inst_server.py.
    def connection_made(self, transport):
        self.transport = transport
        self.inst_queue = asyncio.Queue()
        self.success = 0
        self.failure = 0


    def request_all(self):
        # Request all available instrument data.
        msg = {'request_type' : 'all'}
        json_msg = json.dumps(msg)
        encoded_msg = json_msg.encode()

        self.transport.write(encoded_msg)


    def data_recieved(self, data):
        print('Client: data recieved from server.')
        json_msg = data.decode()
        inst = json.loads(json_msg)
        self.inst_queue.put_nowait(inst)


    def connection(self, exc):
        print('Client: Connection closed.\n\n')


    def request_inst(self, inst_ids):
        # Request a set of instrument data.
        for inst_id in inst_ids:
            print('Client: Requestion inst_id: {}.'.format(inst_id))

        msg = {'request_type' : 'set', 'inst_ids' : inst_ids}
        json_msg = json.dumps(msg)
        encoded_msg = json_msg.encode()

        self.transport.write(encoded_msg)
