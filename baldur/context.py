#!/usr/bin/env python3.5
'''
File: context.py
Auth: Jeremy Butcheck
Orig: 08/13/16
Project: Baldur
'''

from configparser import ConfigParser


def add_args_to_cfg(args, cfg):
    # Inserts parsed arguments into cfg object. 
    args = vars(args)

    for section in cfg.sections():
        for field in cfg[section].keys():
            if field in args.keys() and args[field]:
                cfg[section][field] = args[field]
    try:
        if args.host or args.port or args.rcpt or args.sndr:
            cfg['email']['enabled'] = 'True' 
    except AttributeError:
        pass

    return cfg


def gen_cfg(args):
    # Creates cfg object from ini file.  Further updates the cfg object using 
    # the cmd line arguments.  Returns cfg object. 
    cfg = ConfigParser()
    cfg.read_file(args.ini)

    cfg = add_args_to_cfg(args, cfg)

    return cfg
