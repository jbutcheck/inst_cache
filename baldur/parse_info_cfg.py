#!/usr/bin/env python3


class PropTree(object):
    def __init__(self):
        self.data = None
        self._items = {}

    def append_tree(self, key):
        pt = PropTree()
        self._items[key] = pt
        return pt

    def _get_child(self, keys, skey, default):
        if keys:
            k = keys[0]
        else:
            return self
        pt = self._items.get(k)
        if pt is not None:
            return pt._get_child(keys[1:], skey, default)
        return default

    def get_child(self, skey, default = None):
        keys = skey.split('.')
        return self._get_child(keys, skey, default)

    def get(self, skey, default = None):
        pt = self.get_child(skey)
        if pt is not None:
            return pt.data
        return default
        
    def __getitem__(self, skey):
        pt = self.get_child(skey)
        if pt is not None:
            return pt.data
        else:
            raise KeyError(skey)

    def __contains__(self, skey):
        pt = self.get_child(skey)
        return pt is not None
    
    def __iter__(self):
        return iter(self._items.items())
    
    def __len__(self):
        return len(self._items)
    
    def __str__(self):
        s = ''
        if self.data is not None:
            s = '"%s"' % self.data
        if self._items:
            s += '{'
            for key, pt in self._items.items():
                s += key
                s += ' %s\n' % pt
            s += '}'
        return s
                
            
def _read_string(orig_line):
    line = orig_line.lstrip()
    if '\"' == line[0]:
        line = line[1:]
        sz = 0
        escaped = False
        for c in line:
            if '\"' == c and not escaped:
                break
            escaped = not escaped and '\\' == c
            sz += 1
        if len(line) >= sz and line[sz] == '\"':
            return line[:sz], line[sz + 1:]
        else:
            raise ValueError('Unexpected end of string line: <<%s>> <<%s>>' 
                             % (orig_line, line[sz]))
    else:
        raise ValueError('Expected \"" for line: <<%s>>' % orig_line)

    
def _read_word(line):
    line = line.lstrip()
    sz = 0
    for c in line:
        if c.isspace() or ';' == c:
            break
        sz += 1
    return line[:sz], line[sz:]


def _read_key(line):
    line = line.lstrip()
    if line and '\"' == line[0]:
        return _read_string(line)
    else:
        return _read_word(line)


def _read_data(line):
    line = line.lstrip()
    if line and '\"' == line[0]:
        return _read_string(line)
    else:
        return _read_word(line)


def _read_info_internal_from_iter(fname, lines, pt, depth):
    EXPECT_KEY = 1
    EXPECT_DATA = 2
    EXPECT_DATA_CONT = 3
    state = EXPECT_KEY
    last_pt = None
    pt_stack = [pt]
    line_no = 0
    for orig_line in lines:
        line_no += 1
        line = orig_line.lstrip()
        if line:
            if '#' == line[0]:
                directive, line = _read_word(line[1:])
                if 'include' == directive:
                    if depth > 100:
                        raise ValueError(('Include depth too large, probably' 
                                          'due to recursive include in file '
                                          '%s, line #%d: <<%s>>' % (fname, 
                                                        line_no, orig_line)))
                    inc_fname, line = _read_data(line)
                    _read_info_internal_from_file(inc_fname, pt_stack[-1], 
                                                  depth + 1)
                else:
                    raise ValueError('Unknown directive on line #%d: <<%s>>' 
                                     % (line_no, orig_line))
                continue
            while line:
                line = line.lstrip()
                if not line or ';' == line[0]:
                    if EXPECT_DATA == state:
                        state = EXPECT_KEY
                    break
                if EXPECT_KEY == state:
                    if '{' == line[0]:
                        if last_pt is None:
                            raise ValueError('Unexpected { on line #%d: <<%s>>' 
                                             % (line_no, orig_line))
                        pt_stack.append(last_pt)
                        last_pt = None
                        line = line[1:]
                    elif '}' == line[0]:
                        if len(pt_stack) <= 1:
                            raise ValueError('Unmatched } on line #%d: <<%s>>' 
                                             % (line_no, orig_line))
                        pt_stack.pop()
                        last_pt = None
                        line = line[1:]
                    else: # Key text found
                        key, line = _read_key(line)
                        last_pt = pt_stack[-1]
                        last_pt = last_pt.append_tree(key)
                        state = EXPECT_DATA
                elif EXPECT_DATA == state:
                    if '{' == line[0]:
                        pt_stack.append(last_pt)
                        last_pt = None
                        line = line[1:]
                        state = EXPECT_KEY
                    elif '}' == line[0]:
                        if len(pt_stack) <= 1:
                            raise ValueError('Unmatched } on line #%d: <<%s>>' 
                                             % (line_no, orig_line))
                        pt_stack.pop()
                        last_pt = None
                        line = line[1:]
                        state = EXPECT_KEY
                    else: # Data text found
                        data, line = _read_data(line)
                        last_pt.data = data
                        state = EXPECT_KEY
                elif EXPECT_DATA_CONT == state:
                    pass
                else:
                    raise ValueError('Unknown parser state: <<%s>>' % state)
    # Check if stack has initial size, otherwise some {'s have not been closed
    if len(pt_stack) != 1:
        raise ValueError('Unmatched { on line #%d: <<%s>>' % (line_no, 
                                                              orig_line))
    return pt


def _read_info_internal_from_file(fname, pt, depth):
    with open(fname, 'r') as f:
        return _read_info_internal_from_iter(fname, f, pt, depth)


def read_info_cfg(fname):
    return _read_info_internal_from_file(fname, PropTree(), 0)


def read_info_cfg_str(lines):
    return _read_info_internal_from_iter('', lines.splitlines(), PropTree(), 0)


if __name__ == '__main__':
    cfg = '''
    roo {
        foo 123
        baz {
            gnn "gnn"
            goo "goo"
        }
        d {
            something 4.67
            thank "Thank you very much"
            bank
        }
    }
    '''
    pt = read_info_cfg_str(cfg)
    print('roo', pt.get_child('roo'))
    print('roo.foo', type(pt['roo.foo']))
    print('roo.baz.gnn', pt['roo.baz.gnn'])
    print('roo.baz.goo', pt['roo.baz.goo'])
    print('roo.d.something', pt['roo.d.something'])
    print('roo.d.thank', pt['roo.d.thank'])
    print('get roo.d.thank', pt.get('roo.d.thank'))
    print('get roo.d.bank', pt.get('roo.d.bank'))
    print('contains roo.d.bank', ('roo.d.bank' in pt))
    print('contains roo', ('roo' in pt))
    print('contains roo.d.thank', ('roo.d.thank' in pt))
    print('------------')
    subpt = pt.get_child('roo.baz')
    for k, pt in subpt:
        print(k, pt.data)
