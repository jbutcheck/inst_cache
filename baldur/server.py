#!/usr/bin/env python3.5
'''
File: server.py
Auth: Jeremy Butcheck
Orig: 08/13/16
Project: Baldur
'''

import asyncio
import json
from baldur.mongo import mongo_collection


class Server(asyncio.Protocol):
    # TCP/IP socket server.  Forwards instrument data from mongodb.
    def __init__(self, cfg, loop=None):
        if loop:
            self.loop=loop
        else:
            self.loop = asyncio.get_event_loop()

        self.collection = mongo_collection(cfg)


    def connection_made(self, transport):
        self.transport = transport
        self.peername = transport.get_extra_info("peername")
        print("Server: Connection made: {}.".format(self.peername))


    def connection_lost(self, ex):
        print("Server: Connection lost: {}.".format(self.peername))


    async def _get_insts(self, queue, request_set=None):
        # Get instrument data from mongo and places it in the queue.
        if request_set:
            for inst in request_set:
                await self.collection.find(request)
                queue.put_nowait(request)
        else:
            async for document in self.collection.find():
                queue.put_nowait(document)

        queue.put_nowait(None)


    async def _forward_from_queue(self, queue):
        # Pops data from queue and writes it to transport. Returns on a null
        # value popped from queue.
        msg = await queue.get()
        while msg:
            json_msg = json.dumps(msg)
            encoded_msg = json_msg.encode()
            self.transport.write(encoded_msg)
            msg = await queue.get()


    async def _forward(self, request=None):
        queue = asyncio.Queue()

        task1 = self._forward_from_queue(queue)
        task2 = self._get_insts(queue, request)

        await asyncio.gather(task1, task2)
        print('ALL DONE!')


    def data_received(self, data):
        # Services client request.
        request = json.loads(data.decode())
        print("Server: Request recieved: {}.".format(request))

        if request['request_type'] == 'set':
            request_set = request['inst_ids']
            self.loop.create_task(self._forward(request=request_set))
        if request['request_type'] == 'all':
            self.loop.create_task(self._forward())
