#!/usr/bin/env python3.5
'''
File: load_file.py
Auth: Jeremy Butcheck
Orig: 08/23/16
Project: Baldur
'''

import sys
from os import path as p 
from asyncio import get_event_loop
from argparse import ArgumentParser, FileType

BIN_DIR = p.dirname(p.abspath(sys.argv[0]))
BASE_DIR = p.dirname(BIN_DIR)

sys.path.insert(0, BASE_DIR)

from baldur.context import gen_cfg
from baldur.logging import send_email
from baldur.loader import get_insts, insert_insts


parser = argparse.ArgumentParser(description='Baldur loader. ')
parser.add_argument('-c', '--cfg', dest='ini', required=True,
                    help='A valid ini file.', type=argparse.FileType('r'))
parser.add_argument('-i', '--info', dest='info_file', required=True)

email_group = parser.add_argument_group('email')
email_group.add_argument('--sndr', dest='sndr', help='Sender email address.')
email_group.add_argument('--host', dest='host', help='SMTP host.')
email_group.add_argument('--port', dest='port', help='SMTP port.')
email_group.add_argument('--rcpt', dest='rcpt', help='Recipient email address.')

args = parser.parse_args()
cfg = gen_cfg(args)

loop = get_event_loop()

collection = get_collection(cfg) 

try:
    insts = loop.run_until_complete(get_insts(args.info_file))
    loop.run_until_complete(insert_insts(insts, collection))
except KeyboardInterrupt:
    pass
except Exception as e:
    email = cfg.get('email', 'enabled')
    if email == 'True':
        send_email(cfg, e)
