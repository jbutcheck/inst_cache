#!/usr/bin/env python3.5
'''
File: run_server.py
Auth: Jeremy Butcheck
Orig: 08/23/16
Project: Baldur
'''

import sys
from os import path as p 
from asyncio import get_event_loop
from argparse import ArgumentParser, FileType

BIN_DIR = p.dirname(p.abspath(sys.argv[0]))
BASE_DIR = p.dirname(BIN_DIR)

sys.path.insert(0, BASE_DIR)

from baldur.server import Server
from baldur.context import gen_cfg


parser = ArgumentParser(description='Baldur instrument data server.')
parser.add_argument('-c', '--cfg', dest='ini', required=True,
                    help='A valid ini file.', type=FileType('r'))
parser.add_argument('-p', '--port', dest='info_file', default='9096',
                    help='Port that server will listen on.')

args = parser.parse_args()

loop = get_event_loop()
cfg = gen_cfg(args)

port = cfg.get('server', 'port')
coro = loop.create_server(lambda: Server(cfg), port=port)
server = loop.run_until_complete(coro)

try:
    loop.run_forever()
except KeyboardInterrupt:
    pass

server.close()
loop.run_until_complete(server.wait_closed())
